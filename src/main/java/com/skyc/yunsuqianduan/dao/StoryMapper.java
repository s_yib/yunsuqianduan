package com.skyc.yunsuqianduan.dao;

import org.mybatis.spring.annotation.MapperScan;

import com.skyc.yunsuqianduan.model.Story;
@MapperScan
public interface StoryMapper {
    int deleteByPrimaryKey(Integer storyId);

    int insert(Story record);

    int insertSelective(Story record);

    Story selectByPrimaryKey(Integer storyId);

    int updateByPrimaryKeySelective(Story record);

    int updateByPrimaryKeyWithBLOBs(Story record);

    int updateByPrimaryKey(Story record);
}