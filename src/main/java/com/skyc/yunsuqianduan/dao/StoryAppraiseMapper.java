package com.skyc.yunsuqianduan.dao;

import org.mybatis.spring.annotation.MapperScan;

import com.skyc.yunsuqianduan.model.StoryAppraise;
@MapperScan
public interface StoryAppraiseMapper {
    int deleteByPrimaryKey(Integer appraiseId);

    int insert(StoryAppraise record);

    int insertSelective(StoryAppraise record);

    StoryAppraise selectByPrimaryKey(Integer appraiseId);

    int updateByPrimaryKeySelective(StoryAppraise record);

    int updateByPrimaryKeyWithBLOBs(StoryAppraise record);

    int updateByPrimaryKey(StoryAppraise record);
}