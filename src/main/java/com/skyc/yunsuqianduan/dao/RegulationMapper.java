package com.skyc.yunsuqianduan.dao;

import org.mybatis.spring.annotation.MapperScan;

import com.skyc.yunsuqianduan.model.Regulation;
@MapperScan
public interface RegulationMapper {
    int deleteByPrimaryKey(Integer regulationId);

    int insert(Regulation record);

    int insertSelective(Regulation record);

    Regulation selectByPrimaryKey(Integer regulationId);

    int updateByPrimaryKeySelective(Regulation record);

    int updateByPrimaryKeyWithBLOBs(Regulation record);

    int updateByPrimaryKey(Regulation record);
    
    Regulation findByHouseId(Integer houseId);
}