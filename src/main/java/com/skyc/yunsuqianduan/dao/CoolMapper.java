package com.skyc.yunsuqianduan.dao;

import org.mybatis.spring.annotation.MapperScan;

import com.skyc.yunsuqianduan.model.Cool;

@MapperScan
public interface CoolMapper {
    int deleteByPrimaryKey(Integer coolId);

    int insert(Cool record);

    int insertSelective(Cool record);

    Cool selectByPrimaryKey(Integer coolId);

    int updateByPrimaryKeySelective(Cool record);

    int updateByPrimaryKey(Cool record);
}