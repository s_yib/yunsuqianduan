package com.skyc.yunsuqianduan.dao;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.skyc.yunsuqianduan.model.HouseAppraise;
@MapperScan
public interface HouseAppraiseMapper {
    int deleteByPrimaryKey(Integer appraiseId);

    int insert(HouseAppraise record);

    int insertSelective(HouseAppraise record);

    HouseAppraise selectByPrimaryKey(Integer appraiseId);

    int updateByPrimaryKeySelective(HouseAppraise record);

    int updateByPrimaryKeyWithBLOBs(HouseAppraise record);

    int updateByPrimaryKey(HouseAppraise record);
    
    List<HouseAppraise > findByHouseId(Integer houseId);
}