package com.skyc.yunsuqianduan.dao;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.skyc.yunsuqianduan.model.Province;
@MapperScan
public interface ProvinceMapper {
    int deleteByPrimaryKey(Integer provinceId);

    int insert(Province record);

    int insertSelective(Province record);

    Province selectByPrimaryKey(Integer provinceId);

    int updateByPrimaryKeySelective(Province record);

    int updateByPrimaryKey(Province record);
	List<Province> findAll();
}