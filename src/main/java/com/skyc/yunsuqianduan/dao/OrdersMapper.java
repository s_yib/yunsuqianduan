package com.skyc.yunsuqianduan.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import com.skyc.yunsuqianduan.model.Orders;
@MapperScan
public interface OrdersMapper {
    int deleteByPrimaryKey(Integer ordersId);

    int insert(Orders record);

    int insertSelective(Orders record);

    Orders selectByPrimaryKey(Integer ordersId);

    int updateByPrimaryKeySelective(Orders record);

    int updateByPrimaryKey(Orders record);
    
    List<Orders> findAllOfMyOrdersByUserId(@Param("userId") Integer userId);
    
    List<Orders> findMyHousesOrdersByUserId(@Param("userId") Integer userId);

	void changeHouseRe1Yes(@Param("ordersId") Integer ordersId,@Param("re1") String re1);
	
	void changeHouseRe1No(@Param("ordersId") Integer ordersId,@Param("re1") String re1);
    
    
}