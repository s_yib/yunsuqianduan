package com.skyc.yunsuqianduan.dao;

import org.mybatis.spring.annotation.MapperScan;

import com.skyc.yunsuqianduan.model.StoryResource;
@MapperScan
public interface StoryResourceMapper {
    int deleteByPrimaryKey(Integer resourceId);

    int insert(StoryResource record);

    int insertSelective(StoryResource record);

    StoryResource selectByPrimaryKey(Integer resourceId);

    int updateByPrimaryKeySelective(StoryResource record);

    int updateByPrimaryKey(StoryResource record);
}