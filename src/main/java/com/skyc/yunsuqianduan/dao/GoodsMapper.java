package com.skyc.yunsuqianduan.dao;

import org.mybatis.spring.annotation.MapperScan;

import com.skyc.yunsuqianduan.model.Goods;
@MapperScan
public interface GoodsMapper {
    int deleteByPrimaryKey(Integer goodsId);

    int insert(Goods record);

    int insertSelective(Goods record);

    Goods selectByPrimaryKey(Integer goodsId);

    int updateByPrimaryKeySelective(Goods record);

    int updateByPrimaryKey(Goods record);
}