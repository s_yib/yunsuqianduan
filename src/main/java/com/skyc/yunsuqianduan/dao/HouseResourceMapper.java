package com.skyc.yunsuqianduan.dao;

import org.mybatis.spring.annotation.MapperScan;

import com.skyc.yunsuqianduan.model.HouseResource;
@MapperScan
public interface HouseResourceMapper {
    int deleteByPrimaryKey(Integer resourceId);

    int insert(HouseResource record);

    int insertSelective(HouseResource record);

    HouseResource selectByPrimaryKey(Integer resourceId);

    int updateByPrimaryKeySelective(HouseResource record);

    int updateByPrimaryKey(HouseResource record);
}