package com.skyc.yunsuqianduan.dao;

import org.mybatis.spring.annotation.MapperScan;

import com.skyc.yunsuqianduan.model.HotStory;
@MapperScan
public interface HotStoryMapper {
    int deleteByPrimaryKey(Integer hotStoryId);

    int insert(HotStory record);

    int insertSelective(HotStory record);

    HotStory selectByPrimaryKey(Integer hotStoryId);

    int updateByPrimaryKeySelective(HotStory record);

    int updateByPrimaryKey(HotStory record);
}