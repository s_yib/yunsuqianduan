package com.skyc.yunsuqianduan.dao;

import com.skyc.yunsuqianduan.model.Landlord;

public interface LandlordMapper {
    int deleteByPrimaryKey(Integer landlordId);

    int insert(Landlord record);

    int insertSelective(Landlord record);

    Landlord selectByPrimaryKey(Integer landlordId);

    int updateByPrimaryKeySelective(Landlord record);

    int updateByPrimaryKey(Landlord record);
}