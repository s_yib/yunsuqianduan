package com.skyc.yunsuqianduan.dao;

import org.mybatis.spring.annotation.MapperScan;

import com.skyc.yunsuqianduan.model.HotCity;
@MapperScan
public interface HotCityMapper {
    int deleteByPrimaryKey(Integer hotCityId);

    int insert(HotCity record);

    int insertSelective(HotCity record);

    HotCity selectByPrimaryKey(Integer hotCityId);

    int updateByPrimaryKeySelective(HotCity record);

    int updateByPrimaryKey(HotCity record);
}