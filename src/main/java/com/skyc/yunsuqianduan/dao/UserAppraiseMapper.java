package com.skyc.yunsuqianduan.dao;

import org.mybatis.spring.annotation.MapperScan;

import com.skyc.yunsuqianduan.model.UserAppraise;
@MapperScan
public interface UserAppraiseMapper {
    int deleteByPrimaryKey(Integer appraiseId);

    int insert(UserAppraise record);

    int insertSelective(UserAppraise record);

    UserAppraise selectByPrimaryKey(Integer appraiseId);

    int updateByPrimaryKeySelective(UserAppraise record);

    int updateByPrimaryKey(UserAppraise record);
}