package com.skyc.yunsuqianduan.dao;

import org.mybatis.spring.annotation.MapperScan;

import com.skyc.yunsuqianduan.model.RentType;
@MapperScan
public interface RentTypeMapper {
    int deleteByPrimaryKey(Integer rentTypeId);

    int insert(RentType record);

    int insertSelective(RentType record);

    RentType selectByPrimaryKey(Integer rentTypeId);

    int updateByPrimaryKeySelective(RentType record);

    int updateByPrimaryKey(RentType record);
}