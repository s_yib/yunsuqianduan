package com.skyc.yunsuqianduan.dao;

import org.mybatis.spring.annotation.MapperScan;

import com.skyc.yunsuqianduan.model.SuperAdmin;
@MapperScan
public interface SuperAdminMapper {
    int deleteByPrimaryKey(Integer adminId);

    int insert(SuperAdmin record);

    int insertSelective(SuperAdmin record);

    SuperAdmin selectByPrimaryKey(Integer adminId);

    int updateByPrimaryKeySelective(SuperAdmin record);

    int updateByPrimaryKey(SuperAdmin record);
}