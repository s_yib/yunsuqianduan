package com.skyc.yunsuqianduan.dao;

import org.mybatis.spring.annotation.MapperScan;

import com.skyc.yunsuqianduan.model.County;
@MapperScan
public interface CountyMapper {
    int deleteByPrimaryKey(Integer countyId);

    int insert(County record);

    int insertSelective(County record);

    County selectByPrimaryKey(Integer countyId);

    int updateByPrimaryKeySelective(County record);

    int updateByPrimaryKey(County record);
}