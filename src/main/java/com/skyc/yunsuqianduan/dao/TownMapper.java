package com.skyc.yunsuqianduan.dao;

import org.mybatis.spring.annotation.MapperScan;

import com.skyc.yunsuqianduan.model.Town;
@MapperScan
public interface TownMapper {
    int deleteByPrimaryKey(Integer townId);

    int insert(Town record);

    int insertSelective(Town record);

    Town selectByPrimaryKey(Integer townId);

    int updateByPrimaryKeySelective(Town record);

    int updateByPrimaryKey(Town record);
}