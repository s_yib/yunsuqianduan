package com.skyc.yunsuqianduan.dao;

import org.mybatis.spring.annotation.MapperScan;

import com.skyc.yunsuqianduan.model.UserResource;
@MapperScan
public interface UserResourceMapper {
    int deleteByPrimaryKey(Integer resourceId);

    int insert(UserResource record);

    int insertSelective(UserResource record);

    UserResource selectByPrimaryKey(Integer resourceId);

    int updateByPrimaryKeySelective(UserResource record);

    int updateByPrimaryKey(UserResource record);
}