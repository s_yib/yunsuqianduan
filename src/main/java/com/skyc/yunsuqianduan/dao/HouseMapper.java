package com.skyc.yunsuqianduan.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import com.skyc.yunsuqianduan.model.House;
@MapperScan
public interface HouseMapper {
	long findCount();
	List<House> findAll();
	List<House> findSome();
	List<House> findByTownName(@Param("townName") String townName);
	List<House> findByConditions(@Param("roomer") Integer roomer,@Param("rentTypeId") Integer rentTypeId, 
			@Param("lowPrice") Integer lowPrice,@Param("highPrice") Integer highPrice,@Param("bedroom") Integer bedroom,
			@Param("beds") Integer beds, @Param("toilet") Integer toilet);
   
	int deleteByPrimaryKey(Integer houseId);

    int insert(House record);

    int insertSelective(House record);

    House selectByPrimaryKey(Integer houseId);

    int updateByPrimaryKeySelective(House record);

    int updateByPrimaryKeyWithBLOBs(House record);

    int updateByPrimaryKey(House record);
    
    List<House> findBySouSuo(@Param("province")String province, @Param("renshu")String renshu, @Param("startTime")String startTime,@Param("endTime")String endTime);
	
    List<House> findByNotOrders();
    
	
	
	
}