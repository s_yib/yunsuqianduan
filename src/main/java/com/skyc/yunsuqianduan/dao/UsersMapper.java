package com.skyc.yunsuqianduan.dao;

import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import com.skyc.yunsuqianduan.model.Users;
@MapperScan
public interface UsersMapper  {
    int deleteByPrimaryKey(Integer userId);

    int insert(Users record);

    int insertSelective(Users record);

    Users selectByPrimaryKey(Integer userId);

    int updateByPrimaryKeySelective(Users record);

    int updateByPrimaryKey(Users record);
   
    Users findByUsernameAndPwd(@Param("username") String username, @Param("pwd") String pwd);

	Users findByUsername(String username);
}