package com.skyc.yunsuqianduan.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.skyc.yunsuqianduan.biz.HouseAppraiseBiz;
import com.skyc.yunsuqianduan.biz.HouseBiz;
import com.skyc.yunsuqianduan.biz.HouseTypeBiz;
import com.skyc.yunsuqianduan.biz.LandlordBiz;
import com.skyc.yunsuqianduan.biz.RegulationBiz;
import com.skyc.yunsuqianduan.biz.RentTypeBiz;
import com.skyc.yunsuqianduan.biz.UsersBiz;
import com.skyc.yunsuqianduan.model.House;
import com.skyc.yunsuqianduan.model.HouseAppraise;
import com.skyc.yunsuqianduan.model.HouseType;
import com.skyc.yunsuqianduan.model.Landlord;
import com.skyc.yunsuqianduan.model.Regulation;
import com.skyc.yunsuqianduan.model.RentType;
import com.skyc.yunsuqianduan.model.Users;

@Controller
@RequestMapping("/house")
public class HouseController {
	@Resource
	private HouseBiz houseBiz;
	@Resource
	private RentTypeBiz rentTypeBiz;
	@Resource
	private LandlordBiz landlordBiz;
	@Resource
	private UsersBiz usersBiz;
	@Resource
	private RegulationBiz regulationBiz;
	@Resource
	private HouseAppraiseBiz houseAppraiseBiz;
	@Resource
	private HouseTypeBiz houseTypeBiz;
	
	
	@RequestMapping("/oneHouseNews")
	public String oneHouseNews(Integer houseId,Map<String, Object> request,Integer rentTypeId){
		House oneHouse = houseBiz.OneHouseNews(houseId);
		RentType rentType = rentTypeBiz.findById(rentTypeId);
		Landlord landlord =  landlordBiz.findById(oneHouse.getLandlord().getLandlordId());
		Users user = usersBiz.findById(landlord.getUser().getUserId());
		Regulation regulation = regulationBiz.findByHouseId(houseId);
		List<HouseAppraise> houseAppraise = houseAppraiseBiz.findByHouseId(houseId);
		HouseType houseType = houseTypeBiz.findById(oneHouse.getHouseType().getHouseTypeId());
		request.put("houseType", houseType);
		request.put("appraiseList", houseAppraise);
		request.put("regulation", regulation);
		request.put("landLord", landlord);
		request.put("user", user);
		request.put("rentType", rentType);
		request.put("oneHouse", oneHouse);
		return "/house/oneHouse";	
	}
	

	@RequestMapping("/houseConditions")
	public String findByConditions(Integer roomer,Integer rentType,Integer lowPrice,Integer highPrice,
			Integer bedroom,Integer beds,Integer toilet,Model model){
		List<House> list = houseBiz.findByConditions(roomer,rentType,lowPrice,highPrice,bedroom,beds,toilet);
		model.addAttribute("conditionsList", list);
		return "/house/houseList";	
	}
	
	@RequestMapping("/houseList")
	public String findAll(Map<String, Object> request){
		List<House> list = houseBiz.findSome();
		request.put("list", list);
		return "/house/houseList";	
	}
	
	@RequestMapping("/houseTownList")
	public String findByTownName(String townName,Map<String, Object> request){	
		List<House> list = houseBiz.findByTownName(townName);
		request.put("whereHouseList", list);
		return "/house/houseList";
	}
	
	@RequestMapping("/gaoDeMap")
	public String jump(){	
		return "/house/gaoDeMap";
	}
	
	
	
	@RequestMapping("/sousuo")
	public String sousuo(Map<String, Object> req,HttpServletRequest request,HttpSession session){
		String province = request.getParameter("address");
		String renshu = request.getParameter("renshu");
		String shijian= request.getParameter("shijian");
		String startTime=null;
		String endTime=null;
		if(shijian!=null&&shijian!=""){
		String[] time=shijian.split(" - ");
		 startTime=time[0];
		 endTime=time[1];
		}

		List<House> notOrdersList=houseBiz.findByNotOrders();
		
		List<House> orderList = houseBiz.findBySouSuo(province,renshu,startTime,endTime);
		for (House orderslist : orderList) {
			notOrdersList.add(orderslist);
		}
		
		
		for (House house : notOrdersList) {
			System.out.println(house);
		}
		
		
		req.put("list", notOrdersList);
		
		return "/house/houseList";
	}
	

}
