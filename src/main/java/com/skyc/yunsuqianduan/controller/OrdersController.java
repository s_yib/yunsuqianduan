package com.skyc.yunsuqianduan.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.skyc.yunsuqianduan.biz.HouseBiz;
import com.skyc.yunsuqianduan.biz.OrdersBiz;
import com.skyc.yunsuqianduan.biz.UsersBiz;
import com.skyc.yunsuqianduan.model.House;
import com.skyc.yunsuqianduan.model.Orders;
import com.skyc.yunsuqianduan.model.Users;

import io.goeasy.GoEasy;
@Controller
@RequestMapping("/orders")
public class OrdersController {
	@Resource
	private HouseBiz houseBiz;
	@Resource
	private OrdersBiz ordersBiz;
	@Resource
	private UsersBiz usersBiz;

	//客户出行（作为客人）
	@RequestMapping("/addOrder")
	public String getOrders(Integer houseId,Map<String, Object> request,String times,int days,HttpSession session,String remark) throws ParseException{
		//NullPointException可能是未登录 没有session
		
		Users user = (Users) session.getAttribute("user");
		House oneHouse = houseBiz.OneHouseNews(houseId);
		String price = oneHouse.getPrice();
		Integer money = Integer.parseInt(price) * days;
		
		//修改String 日期为  Date类型
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		format.setLenient(false);
		String [] date  = times.split(" - ");
		Date tsb = format.parse(date[0]);
		Date tse = format.parse(date[1]);
		//客户提交订单 
		Orders record = new Orders();
		record.setRemake(remark);
		record.setHouse(houseBiz.findById(houseId));
		record.setUsers(usersBiz.findById(user.getUserId()));
		record.setBeginTime(tsb);
		record.setEndTime(tse);
		record.setRe1("待审核");
		record.setRe2(Integer.toString(money));
		ordersBiz.addOrders(record);
		//查询我所有的订单
		//推送消息
		GoEasy goEasy = new GoEasy("BC-e72c7e0099ea4b8f8eba290e4e6c7a87");
		goEasy.publish("your_channel","您已成功提交订单");
		return "house/oneHouse";
	}

	@RequestMapping("/justListOrders")
	public String listOrders(HttpSession session,Map<String, Object> request){
		try {
			Users user = (Users) session.getAttribute("user");
			List<Orders> ordersList = ordersBiz.findAllOfMyOrdersByUserId(user.getUserId());
			request.put("ordersList", ordersList);	
			request.put("myself", user);
			return "house/myOrders";
		} catch (Exception e) {
			e.printStackTrace();		}
		return "../index";

	}
	
	@RequestMapping("/listMyHousesOrders")
	public String listMyHousesOrders(HttpSession session,Map<String, Object> request){
		try {
			Users user = (Users) session.getAttribute("user");
			user.getUserId();
			List<Orders> myHousesOrdersList = ordersBiz.findMyHousesOrdersByUserId(user.getUserId());
			request.put("myHousesOrdersList", myHousesOrdersList);	
			request.put("myself", user);
			return "house/myHouses";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		GoEasy goEasy = new GoEasy("BC-e72c7e0099ea4b8f8eba290e4e6c7a87");
		goEasy.publish("sign","请登录");
		return "../index";
	}
	
	//房东审核订单
	@RequestMapping("agreeOrRefuse")
	public String agreeOrRefuse(Integer ordersId,String yes,String no){
		if(no == null ){
			String re1 = yes;
			ordersBiz.changeHouseRe1Yes(ordersId,re1);
		}else{
			String re1 = no;
			ordersBiz.changeHouseRe1No(ordersId,re1);
		}
		return "house/myHouses";
	}
	
	
}


