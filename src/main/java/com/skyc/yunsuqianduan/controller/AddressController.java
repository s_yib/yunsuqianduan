package com.skyc.yunsuqianduan.controller;

import java.util.HashMap;
import java.util.List;
import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skyc.yunsuqianduan.biz.AddressBiz;
import com.skyc.yunsuqianduan.model.Province;

@Controller
public class AddressController {
	@Resource
	AddressBiz addressBiz;
	
	@ResponseBody
	@RequestMapping("/getAddress")
	public HashMap<String, List<Province>> getAddress(){
		HashMap<String, List<Province>> map =new HashMap<>();
		List<Province> list = addressBiz.findAll();
		map.put("list",list);
		return map;
	}
}
