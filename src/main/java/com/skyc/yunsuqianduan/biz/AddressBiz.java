package com.skyc.yunsuqianduan.biz;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.skyc.yunsuqianduan.dao.ProvinceMapper;
import com.skyc.yunsuqianduan.model.Province;
@Service
public class AddressBiz {
	@Resource
	ProvinceMapper provinceDAO;
	public List<Province> findAll() {
		
		return provinceDAO.findAll();
	}
	
}
