package com.skyc.yunsuqianduan.biz;


import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.skyc.yunsuqianduan.dao.UsersMapper;
import com.skyc.yunsuqianduan.model.Users;
import com.skyc.yunsuqianduan.util.EncryptHelper;

@Service
public class UsersBiz {
	@Resource
	private UsersMapper usersDAO;
	
	public Users findById(Integer userId){
		return usersDAO.selectByPrimaryKey(userId);
	}
	

	public Users findByUsernameAndPwd(String username, String encryptPwd) {
		String pwd = EncryptHelper.getMD5(encryptPwd);
		
		return usersDAO.findByUsernameAndPwd(username,pwd);
	}
	
	

	public Users findByUsername(String username) {
		Users user=usersDAO.findByUsername(username);
		System.out.println(user);
		return user;
	}



	public void save(Users user) {
		String pwd =EncryptHelper.getMD5(user.getPwd());
		user.setBirthday(new Date());
		user.setPwd(pwd);
		usersDAO.insert(user);
	}
	
	public void zhifusave(Users user) {
		usersDAO.insert(user);
	}
	

}
