package com.skyc.yunsuqianduan.biz;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import com.skyc.yunsuqianduan.dao.HouseMapper;
import com.skyc.yunsuqianduan.model.House;

@Service
public class HouseBiz {
	@Resource
	private HouseMapper houseDAO;
	
	
	public House OneHouseNews(Integer houseId){
		return houseDAO.selectByPrimaryKey(houseId);

}
	
	public List<House> findAll(){
			return houseDAO.findAll();
	
	}
	public List<House> findSome(){
		return houseDAO.findSome();

}
	public List<House> findByTownName(String townName){
		return houseDAO.findByTownName(townName);

}
	public List<House> findByConditions(Integer roomer,Integer rentTypeId,Integer lowPrice,Integer highPrice,Integer bedroom,Integer beds,Integer toilet){
		return houseDAO.findByConditions(roomer,rentTypeId,lowPrice,highPrice,bedroom,beds,toilet);

}
	
/*	public long findCount(){
		try {
			return houseDAO.findCount();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	public void delete(House house){

			houseDAO.deleteByPrimaryKey(house.getHouseId());

	}
	
	public House findById(int houseId){
		
			return houseDAO.selectByPrimaryKey(houseId);
	}
	
	public void update(House house){
		
	}
	
	public void save(House house){
		
	}
	
	public List<House> findBySouSuo(String province, String renshu, String startTime, String endTime) {
		
		return houseDAO.findBySouSuo(province,renshu,startTime,endTime);
	}
	public List<House> findByNotOrders() {
		return houseDAO.findByNotOrders();
	}

	public List<House> findByNotOrders() {
		return houseDAO.findByNotOrders();
	}


}
