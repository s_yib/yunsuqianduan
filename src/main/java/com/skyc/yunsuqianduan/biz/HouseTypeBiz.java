package com.skyc.yunsuqianduan.biz;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.skyc.yunsuqianduan.dao.HouseTypeMapper;
import com.skyc.yunsuqianduan.model.HouseType;

@Service
public class HouseTypeBiz {
	
	@Resource
	private HouseTypeMapper houseTypeDAO;
	
	public HouseType findById(Integer houseTypeId){
		return houseTypeDAO.selectByPrimaryKey(houseTypeId); 
	}

}
