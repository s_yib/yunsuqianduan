package com.skyc.yunsuqianduan.biz;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.skyc.yunsuqianduan.dao.RegulationMapper;
import com.skyc.yunsuqianduan.model.Regulation;

@Service
public class RegulationBiz {
	@Resource
	private RegulationMapper regulationDAO;
	
	public Regulation findByHouseId(Integer houseId){
		return regulationDAO.findByHouseId(houseId);
	}

}
