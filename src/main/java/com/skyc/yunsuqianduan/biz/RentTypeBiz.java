package com.skyc.yunsuqianduan.biz;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.skyc.yunsuqianduan.dao.RentTypeMapper;
import com.skyc.yunsuqianduan.model.RentType;

@Service
public class RentTypeBiz {
	@Resource
	private RentTypeMapper rentTypeDAO;
	
	public RentType findById(Integer rentTypeId){
		return rentTypeDAO.selectByPrimaryKey(rentTypeId);
	}
}
