package com.skyc.yunsuqianduan.biz;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.skyc.yunsuqianduan.dao.LandlordMapper;
import com.skyc.yunsuqianduan.model.Landlord;

@Service
public class LandlordBiz {
	@Resource
	private LandlordMapper landlordDAO;
	
	public Landlord findById(Integer landlordId){
		return landlordDAO.selectByPrimaryKey(landlordId);
	}
	
}
