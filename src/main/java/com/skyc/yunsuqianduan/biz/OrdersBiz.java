package com.skyc.yunsuqianduan.biz;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.skyc.yunsuqianduan.dao.OrdersMapper;
import com.skyc.yunsuqianduan.model.Orders;

@Service
public class OrdersBiz {
	@Resource
	private OrdersMapper ordersDAO;
	
	public void addOrders(Orders record){
		 ordersDAO.insertSelective(record);
	}

	public List<Orders> findAllOfMyOrdersByUserId(Integer userId) {
		// TODO Auto-generated method stub
		return ordersDAO.findAllOfMyOrdersByUserId(userId);
	}
	
	public List<Orders> findMyHousesOrdersByUserId(Integer userId) {
		// TODO Auto-generated method stub
		return ordersDAO.findMyHousesOrdersByUserId(userId);
	}
	
	public void changeHouseRe1Yes(Integer ordersId,String re1) {
		// TODO Auto-generated method stub
		 ordersDAO.changeHouseRe1Yes(ordersId,re1);
	}
	
	public void changeHouseRe1No(Integer ordersId,String re1) {
		// TODO Auto-generated method stub
		 ordersDAO.changeHouseRe1No(ordersId,re1);
	}
}
