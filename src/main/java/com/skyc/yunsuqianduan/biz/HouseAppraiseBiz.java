package com.skyc.yunsuqianduan.biz;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.skyc.yunsuqianduan.dao.HouseAppraiseMapper;
import com.skyc.yunsuqianduan.model.HouseAppraise;

@Service
public class HouseAppraiseBiz {
	@Resource
	private HouseAppraiseMapper houseAppraiseDAO;
	
	public List<HouseAppraise> findByHouseId (Integer houseId){
		return houseAppraiseDAO.findByHouseId(houseId);
	}
	
}
