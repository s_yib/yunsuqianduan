package com.skyc.yunsuqianduan.model;

import java.io.Serializable;

public class House implements Serializable {
	private static final long serialVersionUID = 21312311L;

	private Integer houseId;

    private HouseType houseType;

    private County county;

    private String address;

    private Landlord landlord;

    private RentType rentType;

    private String houseName;

    private String roomer;

    private String bedroom;

    private String beds;

    private String needments;

    private String wifi;

    private String shampoo;

    private String closet;

    private String tv;

    private String heating;

    private String airConditioner;

    private String hairDryer;

    private String swimPool;

    private String kitchen;

    private String washer;

    private String dryer;

    private String carSpace;

    private String massageBathtub;

    private String toilet;

    private String elevator;

    private String price;

    private String re1;

    private String re2;

    private String re3;

    private String introduce;

    public Integer getHouseId() {
        return houseId;
    }

    public void setHouseId(Integer houseId) {
        this.houseId = houseId;
    }

   

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

	public HouseType getHouseType() {
		return houseType;
	}

	public void setHouseType(HouseType houseType) {
		this.houseType = houseType;
	}

	public County getCounty() {
		return county;
	}

	public void setCounty(County county) {
		this.county = county;
	}

	public Landlord getLandlord() {
		return landlord;
	}

	public void setLandlord(Landlord landlord) {
		this.landlord = landlord;
	}

	public RentType getRentType() {
		return rentType;
	}

	public void setRentType(RentType rentType) {
		this.rentType = rentType;
	}

	public String getHouseName() {
        return houseName;
    }

    public void setHouseName(String houseName) {
        this.houseName = houseName == null ? null : houseName.trim();
    }

    public String getRoomer() {
        return roomer;
    }

    public void setRoomer(String roomer) {
        this.roomer = roomer == null ? null : roomer.trim();
    }

    public String getBedroom() {
        return bedroom;
    }

    public void setBedroom(String bedroom) {
        this.bedroom = bedroom == null ? null : bedroom.trim();
    }

    public String getBeds() {
        return beds;
    }

    public void setBeds(String beds) {
        this.beds = beds == null ? null : beds.trim();
    }

    public String getNeedments() {
        return needments;
    }

    public void setNeedments(String needments) {
        this.needments = needments == null ? null : needments.trim();
    }

    public String getWifi() {
        return wifi;
    }

    public void setWifi(String wifi) {
        this.wifi = wifi == null ? null : wifi.trim();
    }

    public String getShampoo() {
        return shampoo;
    }

    public void setShampoo(String shampoo) {
        this.shampoo = shampoo == null ? null : shampoo.trim();
    }

    public String getCloset() {
        return closet;
    }

    public void setCloset(String closet) {
        this.closet = closet == null ? null : closet.trim();
    }

    public String getTv() {
        return tv;
    }

    public void setTv(String tv) {
        this.tv = tv == null ? null : tv.trim();
    }

    public String getHeating() {
        return heating;
    }

    public void setHeating(String heating) {
        this.heating = heating == null ? null : heating.trim();
    }

    public String getAirConditioner() {
        return airConditioner;
    }

    public void setAirConditioner(String airConditioner) {
        this.airConditioner = airConditioner == null ? null : airConditioner.trim();
    }

    public String getHairDryer() {
        return hairDryer;
    }

    public void setHairDryer(String hairDryer) {
        this.hairDryer = hairDryer == null ? null : hairDryer.trim();
    }

    public String getSwimPool() {
        return swimPool;
    }

    public void setSwimPool(String swimPool) {
        this.swimPool = swimPool == null ? null : swimPool.trim();
    }

    public String getKitchen() {
        return kitchen;
    }

    public void setKitchen(String kitchen) {
        this.kitchen = kitchen == null ? null : kitchen.trim();
    }

    public String getWasher() {
        return washer;
    }

    public void setWasher(String washer) {
        this.washer = washer == null ? null : washer.trim();
    }

    public String getDryer() {
        return dryer;
    }

    public void setDryer(String dryer) {
        this.dryer = dryer == null ? null : dryer.trim();
    }

    public String getCarSpace() {
        return carSpace;
    }

    public void setCarSpace(String carSpace) {
        this.carSpace = carSpace == null ? null : carSpace.trim();
    }

    public String getMassageBathtub() {
        return massageBathtub;
    }

    public void setMassageBathtub(String massageBathtub) {
        this.massageBathtub = massageBathtub == null ? null : massageBathtub.trim();
    }

    public String getToilet() {
        return toilet;
    }

    public void setToilet(String toilet) {
        this.toilet = toilet == null ? null : toilet.trim();
    }

    public String getElevator() {
        return elevator;
    }

    public void setElevator(String elevator) {
        this.elevator = elevator == null ? null : elevator.trim();
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price == null ? null : price.trim();
    }

    public String getRe1() {
        return re1;
    }

    public void setRe1(String re1) {
        this.re1 = re1 == null ? null : re1.trim();
    }

    public String getRe2() {
        return re2;
    }

    public void setRe2(String re2) {
        this.re2 = re2 == null ? null : re2.trim();
    }

    public String getRe3() {
        return re3;
    }

    public void setRe3(String re3) {
        this.re3 = re3 == null ? null : re3.trim();
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce == null ? null : introduce.trim();
    }

	@Override
	public String toString() {
		return "House [houseId=" + houseId + ", houseType=" + houseType + ", county=" + county + ", address=" + address
				+ ", landlord=" + landlord + ", rentType=" + rentType + ", houseName=" + houseName + ", roomer="
				+ roomer + ", bedroom=" + bedroom + ", beds=" + beds + ", needments=" + needments + ", wifi=" + wifi
				+ ", shampoo=" + shampoo + ", closet=" + closet + ", tv=" + tv + ", heating=" + heating
				+ ", airConditioner=" + airConditioner + ", hairDryer=" + hairDryer + ", swimPool=" + swimPool
				+ ", kitchen=" + kitchen + ", washer=" + washer + ", dryer=" + dryer + ", carSpace=" + carSpace
				+ ", massageBathtub=" + massageBathtub + ", toilet=" + toilet + ", elevator=" + elevator + ", price="
				+ price + ", re1=" + re1 + ", re2=" + re2 + ", re3=" + re3 + ", introduce=" + introduce + "]";
	}
    
}