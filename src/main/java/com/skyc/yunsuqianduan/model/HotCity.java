package com.skyc.yunsuqianduan.model;

import java.io.Serializable;

public class HotCity implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 421L;

	private Integer hotCityId;

    private Town town;

    public Integer getHotCityId() {
        return hotCityId;
    }

    public void setHotCityId(Integer hotCityId) {
        this.hotCityId = hotCityId;
    }

	public Town getTown() {
		return town;
	}

	public void setTown(Town town) {
		this.town = town;
	}

   
}