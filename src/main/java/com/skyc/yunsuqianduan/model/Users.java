package com.skyc.yunsuqianduan.model;

import java.io.Serializable;
import java.util.Date;

public class Users implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 464641L;

	private Integer userId;

    private String userName;

    private String pwd;

    private String realName;

    private String sex;

    private Date birthday;

    private String tel;

    private String avatar;

    private Date createTime;

    private String creator;

    private Date updateTime;

    private String updator;

    private String remake;

    private String re1;

    private String re2;

    private String re3;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd == null ? null : pwd.trim();
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel == null ? null : tel.trim();
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar == null ? null : avatar.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdator() {
        return updator;
    }

    public void setUpdator(String updator) {
        this.updator = updator == null ? null : updator.trim();
    }

    public String getRemake() {
        return remake;
    }

    public void setRemake(String remake) {
        this.remake = remake == null ? null : remake.trim();
    }

    public String getRe1() {
        return re1;
    }

    public void setRe1(String re1) {
        this.re1 = re1 == null ? null : re1.trim();
    }

    public String getRe2() {
        return re2;
    }

    public void setRe2(String re2) {
        this.re2 = re2 == null ? null : re2.trim();
    }

    public String getRe3() {
        return re3;
    }

    public void setRe3(String re3) {
        this.re3 = re3 == null ? null : re3.trim();
    }

	@Override
	public String toString() {
		return "Users [userId=" + userId + ", userName=" + userName + ", pwd=" + pwd + ", realName=" + realName
				+ ", sex=" + sex + ", birthday=" + birthday + ", tel=" + tel + ", avatar=" + avatar + ", createTime="
				+ createTime + ", creator=" + creator + ", updateTime=" + updateTime + ", updator=" + updator
				+ ", remake=" + remake + ", re1=" + re1 + ", re2=" + re2 + ", re3=" + re3 + "]";
	}
    
}