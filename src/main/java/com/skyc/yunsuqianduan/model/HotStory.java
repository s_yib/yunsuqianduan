package com.skyc.yunsuqianduan.model;

import java.io.Serializable;

public class HotStory implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 321L;

	private Integer hotStoryId;

    private Story story;

    public Integer getHotStoryId() {
        return hotStoryId;
    }

    public void setHotStoryId(Integer hotStoryId) {
        this.hotStoryId = hotStoryId;
    }

	public Story getStory() {
		return story;
	}

	public void setStory(Story story) {
		this.story = story;
	}
   
}