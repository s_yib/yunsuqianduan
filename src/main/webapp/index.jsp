<%@page import="java.util.Date"%>
<%@page import="com.skyc.yunsuqianduan.model.Users"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<!DOCTYPE html >
<html>
    <head>
     <title>云宿</title>
     <base href="<%=request.getContextPath() %>/" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        	<meta name="renderer" content="webkit">
  			
  			
  			<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        	
        	
        	<link rel="stylesheet" href="css/bootstrap.min.css" />
        	<link rel="stylesheet" href="css/daterangepicker-bs2.css" />
			<link rel="stylesheet" href="css/daterangepicker-bs3.css" />
			<link rel="stylesheet" href="css/font-awesome.min.css" />
			<link rel="stylesheet" href="css/index.css" />
			<link rel="stylesheet" href="css/shouye.css" />
			<link rel="stylesheet" href="css/zhuce.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/layui.css"  media="all">
			
			<script type="text/javascript" src="js/bootstrap.min.js" ></script>
        	<script type="text/javascript" src="js/jquery.min.js" ></script>
        	<script type="text/javascript" src="js/daterangepicker.js" ></script>
        	<script type="text/javascript" src="js/jquery-1.8.3.min.js" ></script>
        	<script type="text/javascript" src="js/moment.js" ></script>
        	<script type="text/javascript" src="js/moment.min.js" ></script>
        	<script type="text/javascript" src="js/jquery-1.7.1.min.js" ></script>
        	<script type="text/javascript" src="js/jquery-1.9.0.min.js" ></script>
        	<script type="text/javascript" src="js/yanzheng.js" ></script>
        	<script type="text/javascript" src="js/dengluzhuce.js" ></script>
        	<script type="text/javascript" src="js/sousuokuang.js" ></script>
			<script type="text/javascript" src="layDate-v5.0.2/laydate/laydate.js" ></script>
			<script type="text/javascript" src="https://cdn.goeasy.io/goeasy.js"></script>
			

</head>
    <body>
    <script type="text/javascript">   
var goeasy = new GoEasy({
    appkey: 'BC-e72c7e0099ea4b8f8eba290e4e6c7a87',

     });

goeasy.subscribe({
  channel: 'sign',
onMessage: function (result) {
alert("尊敬的用户 " + result.content);
}
});
         </script> 
    <!--     <a href="house/houseList">房源</a>
        <a href="orders/justListOrders">出行订单列表</a>
        <a href="orders/listMyHousesOrders">房东订单列表</a> -->
    	<div id="bj" class="nav" style="width: 100%; height: 480px; padding: 0;">
			<ul>
			<c:choose>
				<c:when test="<%=session.getAttribute(\"user\")==null%>">
				<li><a href="javascript:void(0)" onClick="showBox()"><span class="underline">登录</span></a></li>
				<li><a href="javascript:void(0)" onClick="zhuCe()"><span class="underline">注册</span></a></li>
				</c:when>
				<c:otherwise>
				<li><a href="#"><span class="underline">个人中心</span></a></li>
				<li><a href="#"><span class="underline">帮助</span></a></li>
				<li><a href="#"><span class="underline">消息</span></a></li>
				<li><a href="orders/justListOrders"><span class="underline">旅程</span></a></li>
				</c:otherwise>
			</c:choose>	
				<li><a href="#"><span class="underline">故事</span></a></li>
				<li><a href="#"><span class="underline">成为房东</span></a></li>
				<li><a href="#"><span class="underline">手机端</span></a></li>
			</ul>
    	</div>
    	<div class="event" id="login_box" style="display: none;">
		<div class="login">
			<div class="title">
				<span class="t_txt">登录</span>
				<span class="del" onClick="deleteLogin()">X</span>
			</div>
			<form action="" id="user" method="post">
				<input type="text" name="userName" id="userName" value="" placeholder="请输入用户名"/>
				<input type="password" name="pwd" id="pwd" value="" placeholder="请输入密码"/>
				<input type="submit" onclick=" denglu() " value="登录" class="btn" />
				<a href="https://openauth.alipay.com/oauth2/appToAppAuth.htm?app_id=2017083108475599&redirect_uri=http://127.0.0.1:8080/yunsuqianduan/return_url.jsp">支付宝登陆</a>
				<a href="javascript:void(0)" class="wapper" onclick='obj.sweep()'>扫码登录</a>
			</form>	
		</div>
	</div>
    	<div class="bg_color" onClick="deleteLogin()" id="bg_filter" style="display: none;"></div>
    	<script src="js/index.js" type="text/javascript" charset="utf-8"></script>
    	
    	
    	
    	
    	<div class="zhuce_event" id="zhuce_box" style="display: none;">
		<div class="zhuce">
			<div class="zhuce_title" style="width: 500px ; height: 80px" >
				<div class="zhuce_del" onClick="deleteZhuce()"><img src="img/guanbi.png" width="27px" height="25px"></div>
			</div>
			<div class="weixin_zhuce">
				<a href="#"><img src="img/weixinzhuce.png" width="500px"  height="50px"></a>
			</div>
			<div class="zhifubao_zhuce">
				<a href="https://openauth.alipay.com/oauth2/appToAppAuth.htm?app_id=2017083108475599&redirect_uri=http://127.0.0.1:8080/yunsuqianduan/return_url.jsp"><img src="img/zhifubaozhuce.png" width="500px"  height="50px"></a>
			</div>
			<div class="weibo_zhuce">
				<a href=""><img src="img/weibozhuce.png" width="500px"  height="50px"></a>
			</div>
			<div class="chuangjian_zhuce">
				<a href="javascript:void(0)" onclick="xinxi_box()" ><img src="img/chuangjianzhanghao.png" width="500px"  height="50px"></a>
			</div>
			
			<div class="shengming" style=" width: 500px;  "  >
				<p>
				点击“注册”或“继续”即表示我同意爱彼迎的<a>服务条款</a>、
				<a>支付服务条款</a>、<a>隐私政策</a>和<a>非歧视政策</a>。
				 如果我是中国居民，我将与云宿中国签约，
				 而且我的信息会根据中国的法律被储存和处理，
				 这些法律包括隐私和信息披露方面的法律。 <a>了解更多</a>
				 </p>
			</div>
		</div>
	</div>
    	<div class="zhuce_bg_color" onClick="deleteZhuce()" id="bg_zhuce" style="display: none;"></div>
    	
    	
    	<div  class="xinxi" id="xinxi_box" style=" display: none;" >
    		<div class="xinxi_neirong">
    			<div class="zhuce_title" style="width: 500px ; height: 80px" >
    			<div class=""></div>
				<div class="xinxi_del" onClick="deleteXinxi()"><img src="img/guanbi.png" width="27px" height="25px"></div>
			
				<form action="" method="post" >
					<div class="xiangxixinxi">
					
						<div class="yonghuming">
							用户名:<input type="text" name="userName" id="userName_zhuce" onblur="yanzhengyonghuming()">
								<div class="zhanghaotishi" id="tishi"></div>
						</div>
						
						<div class="mima">
							密码:<input type="password" name="pwd" id="pwd_zhuce" onblur="yanzhengmima()">
								<div class="mimatishi" id="mimatishi"></div>
						</div>
						
						<div class="querenmima">
							确认密码:<input type="password"  id="pwd_queren" onblur="querenmima()"/>
								<div class="querenmimatishi" id="querenmimatishi"></div>
						</div>
							
						<div class="zhengshixingming">
							姓名:<input type="text" name="realName" class="xingming_zhuce" value=""/>
						</div>
						
						<div class="shengri">
							生日:<input type="date" name="recordTime" value='<fmt:formatDate value="<%=new Date() %>" type="date" pattern="yyyy-MM-dd"/>' >
						</div>
						
						<div class="tel">
							手机:<input type="text" name="tel" class="shouji_zhuce" value=""/>
						</div>
						
						<div class="tijiao">
							<input type="submit"  value="注册" onclick="yonghuzhuce()" />
						</div>
						
					</div>
				</form>				
			</div>
    		</div>
    	</div>
    	
    	
    	<form action="house/sousuo" method="post" id="fromid" >
    	<div class="ssk"  >
    		<div class="ssk_d1">
    			<div class="ssk_d1_d1">
    				地点
    			</div>
    			
    			<div class="ssk_sl">
    			<input class="mdd" style="border: none" name="address" value=""  placeholder="目的地，城市，地址" />
    			</div>
    			
    		</div>
    		<div class="ssk_d2">
    			<div class="ssk_d2_d1">
    				时间
    			</div>
    			<div class="ssk_sl"  >
       				 <input type="text" name="shijian" value="" class="layui-input" id="test6" placeholder="选择时间" style=" width:280px ; border:none;  ">
    			</div>
    		</div>
    		<div class="ssk_d3">
    		
    			<div class="ssk_d3_d1">
    			  
    				<a  href="house/houseList" class="sousuotijiao"><div class="ssk_d3_d1_d1">搜索</div></a>
    			</div>

				
				<div class="ssk_d3_d2">
    				房客	
    			</div>
				<div class="ssk_sl">
    			<input  style="border: none" name="renshu" value=""  placeholder="人数" />
    			</div>
    		</div>
    	</div>
    	</form>
    	
    	
    	<div class="dizhitanchu" id="dizhitanchukuang" style="display: none;">
    		
    	</div>
    	


	<hr style="margin:40px auto; height: 1px; color: #D0E9C6;  " width="1000px"  />
    	
    	<div class="zjll" style="display: none;">
    		
    		<div class="bt_mdd">
    	
    			<h3>最近浏览过的</h3>
    	
    		</div>
    		<div class="mdd_tp_1">
    	
	    		<div class="mdd_tp_1_d1">
	    			<div class="mdd_tp_1_d1_1"><a href="#"><img src="img/rmmdd1.png" width="320px"  height="260px"></a></div>
	    			<div class="mdd_tp_1_d1_2">$300/晚</div>
	    			<div class="mdd_tp_1_d1_3">132条评价</div>
	    		</div>
	    		
	    		<div class="mdd_tp_1_d2">
	    			<div class="mdd_tp_1_d2_1"><a href="#"><img src="img/rmmdd2.png" width="320px" height="260px" ></a></div>
	    			<div class="mdd_tp_1_d2_2">$300/晚</div>
	    			<div class="mdd_tp_1_d2_3">132条评价</div>
	    		</div>
	    		
	    		<div class="mdd_tp_1_d3">
	    			<div class="mdd_tp_1_d3_1"><a href="#"><img src="img/rmmdd3.png" width="320px" height="260px" ></a></div>
	    			<div class="mdd_tp_1_d3_2">$300/晚</div>
	    			<div class="mdd_tp_1_d3_3">132条评价</div>
	    		</div>
    		
    	</div>
    		
    	</div>
    	
    	
    	<div class="bt_mdd">
    	
    	<h3>热门目的地</h3>
    	
    	</div>
    	
    	<div class="mdd_dhl">
    		<ul>
    			<li>北京</li>
    			<li>东京</li>
    			<li>南京</li>
    			<li>西京</li>
    			<li>中京</li>
    		</ul>
    	</div>
    	
    	<hr style="margin: 10px auto; height: 5px; color: #D0E9C6;  " width="1000px"  />
    	
    	<div class="mdd_tp_1">
    	
    		<div class="mdd_tp_1_d1">
    			<div class="mdd_tp_1_d1_1"><a href="#"><img src="img/rmmdd1.png" width="320px"  height="260px"></a></div>
    			<div class="mdd_tp_1_d1_2">$300/晚</div>
    			<div class="mdd_tp_1_d1_3">132条评价</div>
    		</div>
    		
    		<div class="mdd_tp_1_d2">
    			<div class="mdd_tp_1_d2_1"><a href="#"><img src="img/rmmdd2.png" width="320px" height="260px" ></a></div>
    			<div class="mdd_tp_1_d2_2">$300/晚</div>
    			<div class="mdd_tp_1_d2_3">132条评价</div>
    		</div>
    		
    		<div class="mdd_tp_1_d3">
    			<div class="mdd_tp_1_d3_1"><a href="#"><img src="img/rmmdd3.png" width="320px" height="260px" ></a></div>
    			<div class="mdd_tp_1_d3_2">$300/晚</div>
    			<div class="mdd_tp_1_d3_3">132条评价</div>
    		</div>
    		
    	</div>
    	<div class="mdd_tp_1">
    	
    		<div class="mdd_tp_1_d1">
    			<div class="mdd_tp_1_d1_1"><a href="#"><img src="img/rmmdd4.png" width="320px"  height="260px"></a></div>
    			<div class="mdd_tp_1_d1_2">$300/晚</div>
    			<div class="mdd_tp_1_d1_3">132条评价</div>
    		</div>
    		
    		<div class="mdd_tp_1_d2">
    			<div class="mdd_tp_1_d2_1"><a href="#"><img src="img/rmmdd5.png" width="320px" height="260px" ></a></div>
    			<div class="mdd_tp_1_d2_2">$300/晚</div>
    			<div class="mdd_tp_1_d2_3">132条评价</div>
    		</div>
    		
    		<div class="mdd_tp_1_d3">
    			<div class="mdd_tp_1_d3_1"><a href="#"><img src="img/rmmdd6.png" width="320px" height="260px" ></a></div>
    			<div class="mdd_tp_1_d3_2">$300/晚</div>
    			<div class="mdd_tp_1_d3_3">132条评价</div>
    		</div>
    		
    	</div>
    	
    	
    	<div class="row clearfix">
				<div class="col-md-1 column"></div>
				<div class="col-md-4 column">
					<ul style="list-style-type: none;">
						<li style="margin-bottom: 18px; font-size: larger">出行</li>
						<li>信任与安全</li>
						<li>旅行基金</li>
						<li>礼品卡</li>
						<li>商务差旅</li>
						<li>旅游指南</li>

					</ul>
				</div>
				<div class="col-md-4 column">
					<ul style="list-style-type: none;">
						<li style="margin-bottom: 18px; font-size: larger">关于我们</li>
						<li>工作机会</li>
						<li>新闻</li>
						<li>政策</li>
						<li>帮助</li>
						<li>多元化与归属感</li>
					</ul>
				</div>
				<div class="col-md-3 column">
					<ul style="list-style-type: none;">
						<li style="margin-bottom: 18px; font-size: larger">为什么要出租</li>
						<li>房东义务</li>
						<li>房东权益</li>
						<li>待客之道</li>
					</ul>
				</div>
				</div>
		</div>
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
 <script>
lay('#version').html('-v'+ laydate.v);


laydate.render({
  elem: '#test6'
  ,range: true
});
</script>
    	
    	
 	</body>
</html>
 