<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="content-type" >
<title>房屋详情</title>
<base href="<%=request.getContextPath() %>/" />
  <style type="text/css">
  #li1{ display: block; float: left; width:170px; }
  #li2{ display: block; float: left; width:169px; margin-top: 10px;}
  #li3{ display: block; float: left; width:90px; margin-top: 10px;}
  #div2 a{ display: block; height: 30px;line-height: 30px;width: 30px; text-align: center; background-color: #ffffff; margin-right: 1px; text-decoration: none;  }
  #div2 a:hover{ color: black;  margin-right: 1px;  }
  #div3 p{font-size: 16px;margin-top: 30px;margin-left: 40px;}
  .demo-input{padding-left: 10px; height: 38px; min-width: 262px; line-height: 38px; border: 1px solid #e6e6e6;  background-color: #fff;  border-radius: 2px;}
 </style>
</head>
<link
	href="https://cdn.bootcss.com/bootstrap/3.0.1/css/bootstrap-theme.css"
	rel="stylesheet">
<link
	href="https://cdn.bootcss.com/bootstrap/3.0.1/css/bootstrap-theme.min.css"
	rel="stylesheet">
<link href="https://cdn.bootcss.com/bootstrap/3.0.1/css/bootstrap.css"
	rel="stylesheet">
<link
	href="https://cdn.bootcss.com/bootstrap/3.0.1/css/bootstrap.min.css"
	rel="stylesheet">
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/affix.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/affix.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/alert.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/alert.min.js"></script>
<link
	href="https://cdn.bootcss.com/bootstrap/3.0.1/css/bootstrap.min.css"
	rel="stylesheet">
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/affix.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/affix.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/alert.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/alert.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/bootstrap.js"></script>
<script
	src="https://cdn.bootcss.com/bootstrap/3.0.1/js/bootstrap.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/button.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/button.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/carousel.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/collapse.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/collapse.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/dropdown.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/dropdown.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/modal.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/modal.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/popover.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/popover.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/scrollspy.js"></script>
<script
	src="https://cdn.bootcss.com/bootstrap/3.0.1/js/scrollspy.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/tab.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/tooltip.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/tooltip.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/transition.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.0.1/js/transition.min.js"></script>
<script type="text/javascript" src="https://cdn.goeasy.io/goeasy.js"></script>

<link href="css/bootstrap.min.css" rel="stylesheet">

<link href="css/font-awesome.min.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" media="all"
	href="css/daterangepicker-bs3.css" />

<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>

<script type="text/javascript"
	src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

<script type="text/javascript" src="js/moment.js"></script>

<script type="text/javascript" src="js/daterangepicker.js"></script>
<script type="text/javascript" src="js/laydate/laydate.js"></script>




<body>
<script type="text/javascript">   
var goeasy = new GoEasy({
    appkey: 'BC-e72c7e0099ea4b8f8eba290e4e6c7a87',

     });

goeasy.subscribe({
  channel: 'your_channel',
onMessage: function (result) {
alert("尊敬的用户 " + result.content);
}
});
         </script> 


	<div class="container">
		<div class="row clearfix">
			<div class="col-md-1 column">
				<img alt="75x75" src="img/logo.png" />
			</div>
			<div class="col-md-6 column">

				<form action="house/houseTownList" method="post" role="form">
					<div class="form-group" style="margin-top: 32px;">
						<input style="width: 300px;" placeholder="请输入您想去的城市"
							name="townName" type="text" class="form-control" />
					</div>
					<button type="submit" class="btn btn-default"
						style="position: absolute; top: 32px; right: 200px;">搜索</button>
				</form>
			</div>
			<div class="col-md-5 column" style="margin-top: 30px;">
				<ul class="nav nav-pills">
					<li class="#"><a href="#">房东</a></li>
					<li><a href="#">故事</a></li>
					<!--disabled不能点-->
					<li class="active"><a href="orders/justListOrders">旅程</a></li>
					<li class="#"><a href="#">消息</a></li>
					<li class="disabled"><a href="#">头像（个人资料）</a></li>
				</ul>
			</div>
		</div>
	</div>

	
	<div class="container">
		<div class="row clearfix">
			<div class="col-md-12 column">
				<hr />
			</div>
		</div>
	</div>


<div class="container">
	<div class="row clearfix">
	<!-- 房源图片  -->
	<div class="col-md-2 column"></div>
	<div class="col-md-8 column" style="height: 400px"><img src="img/house1.jpg" style="height: 100%" width="100%"/></div>
	<div class="col-md-2 column"></div>
	</div>
</div>
	
			
	<div class="container">
	<div class="row clearfix">
	<div class="col-md-12 column">
		<hr />
	</div>
	</div>
	</div>
	
	<div class="container">
	<div class="row clearfix">
	<div class="col-md-12 column">
	
	<div>
		<div  class="col-md-8 column" >
		<div id="div2">
    		<ul>
        		<li id="li3"><a href="JavaScript:void(0)" onclick="document.getElementById('introduce').scrollIntoView();">综述</a></li>
        		<li id="li3"><a href="JavaScript:void(0)" onclick="document.getElementById('apprise').scrollIntoView();">评价</a></li>
				<li id="li3"><a href="JavaScript:void(0)" onclick="document.getElementById('houseOwner').scrollIntoView();">房东</a></li>
        		<li id="li3"><a href="JavaScript:void(0)" onclick="document.getElementById('address').scrollIntoView();">位置</a></li>
    		</ul>
    	</div>
    	<p style="color: white">$nbsp;$nbsp;$nbsp;$nbsp;$nbsp;$nbsp;$nbsp;$nbsp;</p>
		<hr />
		<br>
		<h2 style="font-weight: bolder; margin-left: 40px;">${oneHouse.houseName}</h2>
		
		<div id="div2" style="padding-top: 20px; ">
    		<ul>
        		<li id="li1"><a href="#"><img src="img/rentType.png"/></a></li>
        		<li id="li1"><a href="#"><img src="img/roomer.png"/></a></li>
        		<li id="li1"><a href="#"><img src="img/bedroom.png"/></a></li>
        		<li id="li1"><a href="#"><img src="img/bed.png"/></a></li>
    		</ul>
    		<ul>
    			<li id="li2">${rentType.typeName }</li>
    			
        		<li id="li2">${oneHouse.roomer}位房客</li>
        		<li id="li2">${oneHouse.bedroom}间卧室</li>
        		<li id="li2">${oneHouse.beds}张床</li>
        		<li id="li2">${rentType.remake }</li>
    		</ul>   
   		 </div>
	
		<br>
		<br>
		<div id="div3" >
		
		
		<p style="font-size: 20px; font-weight: bolder; padding-top: 50px;" id="introduce">综述</p>
		<div  class="col-md-12 column" >
		<hr />
		</div>
		<p style="font-size: 18px; font-weight: bolder; padding-top: 30px;">房屋类型</p>
		<p>${houseType.typeName}</p>
		<p style="font-size: 18px; font-weight: bolder; padding-top: 30px;">房屋简介</p>
		<p>${oneHouse.introduce}</p>
		<p style="font-size: 18px; font-weight: bolder; padding-top: 30px;">房屋守则</p>
		<p>${regulation.remake}</p>
		
		
		
		
		
		<p style="font-size: 20px; font-weight: bolder; padding-top: 50px;" id="apprise">评价</p>
		<div  class="col-md-12 column" >
		<hr />
		</div>
		<c:forEach var="list" items="${appraiseList}">
			<p>${list.userName}:${list.content}</p>
		</c:forEach>
		
		
		
		<p style="font-size: 20px; font-weight: bolder; padding-top: 50px;" id="houseOwner" >房东</p>
		<div  class="col-md-12 column" >
		<hr />
		</div>
		<p ><img src="img/man.png"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;房东:${user.userName}&nbsp;&nbsp;&nbsp;&nbsp;${landLord.remake}</p>
		
		
		
		<p style="font-size: 20px; font-weight: bolder; padding-top: 50px;" id="address">位置</p>
		<div  class="col-md-12 column" >
		<hr />
		</div>
		<p>${oneHouse.address}</p>
		</div>
		</div>
	</div>

		
	<div class="col-md-4 column" id="nomove" style="position: relative;" >
				<div>
							<p align="center" style="background-color: rgba(68,69,69,0.5);color: white;width: 290px;height: 50px;font-size: 30px;">价格：￥${oneHouse.price}每晚</p>
							
							<form  action="orders/addOrder" method="post">
							
							<input type="text" class="demo-input" placeholder="请选择日期" name="times" id="times">
							<input type="hidden"name="days" id="days" value=""/>
							<input type="hidden"name="price" value="${oneHouse.price}" id="price" />
							<input type="hidden"name="houseId" value="${oneHouse.houseId}" />
							<input type="text" name="remark" value="无" />
							<input type="button" value="确定" onClick="date()" >
							<p>总价格</p>
							<input type="hidden" name="finalPrice" id="finalPrice" value="0"></input>
							<button type="submit" style="width: 290px;height: 50px; font-size: 30px; color: white;background-color: #FF7E82" >现在预订！</button>
							</form>
				</div>
	</div>
				<script>

				laydate.render({
				  elem: '#times'
				  ,range: true
				});

				</script>
				<script type="text/javascript">            
              function date(){
               var times = document.getElementById("times").value;
           	   var timess = times.split(" - ");
           	   var beginTime = timess[0];
           	   var endTime = timess[1]; 
           	   var date1Str = beginTime.split("-");
           	   var date1Obj = new Date(date1Str[0],(date1Str[1]-1),date1Str[2]);
           	   var date2Str = endTime.split("-");
           	   var date2Obj = new Date(date2Str[0],(date2Str[1]-1),date2Str[2]);
           	   var t1 = date1Obj.getTime();//返回从1970-1-1开始计算到Date对象中的时间之间的毫秒数
           	   var t2 = date2Obj.getTime();//返回从1970-1-1开始计算到Date对象中的时间之间的毫秒数
           	   var datetime=1000*60*60*24; //一天时间的毫秒值 
           	   var days = Math.floor(((t2-t1)/datetime));//计算出两个日期天数差 
           	   var day = document.getElementById("days");
           	   	day.value = days;
           	   	/* alert(days); */
            	   var price =parseInt(document.getElementById("price").value);  
            	   var money = days * price;
            	   var finalPriceModel =  document.getElementById("finalPrice");
            	   finalPriceModel.type = "text";
            	   finalPriceModel.value = money;
             /*   var str = document.getElementById("date");
               var arr = new Array();
               arr =  str.value.split("-");
               alert(arr[0]); */
               }
              
            /*   function checkSession(){
            	  if(document.getElementById(elementId)){
            		  
            	  }
            	  
              } */

             	var onmove=document.getElementById("nomove");
       			var btn=document.getElementById("btn");
       		window.onscroll=function(){
       			var scroll =document.documentElement.scrollTop||document.body.scrollTop;
       			if(Math.ceil(scroll) >= 550){
       				onmove.style.position='fixed';
       				onmove.style.top="10px";
       				onmove.style.left="865px";
       			}else{
       				onmove.style.position='relative';
       				onmove.style.top="0px";
       				onmove.style.left="0px";
       			}
       		}
              
               </script>
			
		</div>
		
			</div>
		</div>



		<div class="container">
			<div class="row clearfix">
				<div class="col-md-12 column">
				<div style="width:800px">
		 	<div style="woverflow-x:hidden; ">
		<!-- <a href="house/gaoDeMap">地图</a> -->
		<iframe id="gaoDeMap" src="house/gaoDeMap" style=" border:0px;width: 700px;height: 450px; "></iframe>
			</div> 
		
		</div> 
					<div align="center">类似房源</div>
					<hr />
				</div>
			</div>
		</div>

<div>

			<div class="row clearfix">
				<div class="col-md-1 column"></div>
				<div class="col-md-4 column">
					<ul style="list-style-type: none;">
						<li style="margin-bottom: 18px; font-size: larger">出行</li>
						<li>信任与安全</li>
						<li>旅行基金</li>
						<li>礼品卡</li>
						<li>商务差旅</li>
						<li>旅游指南</li>

					</ul>
				</div>
				<div class="col-md-4 column">
					<ul style="list-style-type: none;">
						<li style="margin-bottom: 18px; font-size: larger">关于我们</li>
						<li>工作机会</li>
						<li>新闻</li>
						<li>政策</li>
						<li>帮助</li>
						<li>多元化与归属感</li>
					</ul>
				</div>
				<div class="col-md-3 column">
					<ul style="list-style-type: none;">
						<li style="margin-bottom: 18px; font-size: larger">为什么要出租</li>
						<li>房东义务</li>
						<li>房东权益</li>
						<li>待客之道</li>
					</ul>
				</div>
				</div>
		</div>


	
</body>
</html>